from fastapi import FastAPI, APIRouter

from src.domain.common.routes import ws_router
from src.domain.users.routes import users_router, auth_users_router
from src.infrastructure.routes import AuthRoute
from src.infrastructure.ws.connections_manager import ConnectionManager

app = FastAPI()
auth_router = APIRouter(route_class=AuthRoute)


@app.get('/ping')
async def ping():
    return 'pong'


app.include_router(users_router)
app.include_router(auth_users_router)
app.include_router(ws_router)


app.add_event_handler('startup', ConnectionManager().connect_broadcasters)
app.add_event_handler('shutdown', ConnectionManager().disconnect_broadcasters)
