from pydantic import BaseSettings


class Domains(BaseSettings):
    users: str = 'http://127.0.0.1:8001'
    chat: str = 'http://127.0.0.1:8002'

    class Config:
        env_file = '.env'


class Settings(BaseSettings):
    environment: str = 'local'
    domains: Domains

    redis_url: str = 'redis://127.0.0.1:6379'

    jwt_algorithm: str = 'HS256'
    jwt_access_secret: str
    jwt_refresh_secret: str
    jwt_ws_secret: str

    class Config:
        env_file = '.env'

    @property
    def is_prod(self):
        return self.environment in ('prod', 'production')


def get_settings():
    return Settings(domains=Domains())
