import traceback
from typing import Callable

from pydantic import ValidationError
from starlette.websockets import WebSocket

from src.infrastructure.ws._handlers import handlers_mapper
from src.infrastructure.ws.connections_manager import ConnectionManager
from src.infrastructure.ws.schemas import WSRequest

ws_manager = ConnectionManager()


async def process_websockets_messages(
        websocket: WebSocket,
        user_id: str
):
    try:
        while True:
            try:
                data = await websocket.receive_json()
                print(data)
                ws_request = WSRequest(**data)
            except ValidationError:
                # todo
                ...
            else:
                await handle_request(
                    websocket, user_id, ws_request
                )

    except Exception:
        traceback.print_exc()
        await ws_manager.disconnect(
            user_id=user_id,
            websocket=websocket
        )


async def subscribe_user_topic(websocket, user_id):
    async with ws_manager.broadcaster.subscribe(channel=user_id) as subscriber:
        async for event in subscriber:
            await ws_manager.send_personal_message(
                websocket=websocket,
                response=event.message
            )


def get_handler(key: str) -> Callable:
    keys = key.split('.')
    handler = handlers_mapper
    for _key in keys:
        handler = handler.get(_key)
    return handler


async def handle_request(
        websocket: WebSocket,
        user_id: str,
        data: WSRequest
):
    handler_cls = get_handler(data.request)
    handler = handler_cls(
        websocket,
        user_id,
        data.payload,
        data.request_id
    )
    await handler.run()
