import asyncio
from collections import defaultdict
from typing import List, Dict, Union
from fastapi import WebSocket

from src.infrastructure.types import MetaSingleton
from src.infrastructure.ws.broadcaster import Broadcast
from src.infrastructure.ws.schemas import WSResponse
from src.settings import get_settings


class ConnectionManager(metaclass=MetaSingleton):
    def __init__(self):
        settings = get_settings()
        self.active_connections: Dict[str, List[WebSocket]] = defaultdict(list)
        self.broadcaster = Broadcast(settings.redis_url)

    async def connect_broadcasters(self):
        await self.broadcaster.connect()

    async def disconnect_broadcasters(self):
        await self.broadcaster.disconnect()

    async def connect(
            self,
            websocket: WebSocket,
            user_id: str
    ):
        await websocket.accept()
        await self.add_connection(user_id=user_id, websocket=websocket)

    async def add_connection(self, user_id: str, websocket: WebSocket):
        self.active_connections[user_id].append(websocket)

    async def disconnect(
            self,
            user_id: str,
            websocket: WebSocket
    ):
        self.active_connections[user_id].remove(websocket)

    async def auth_failed_error(self, websocket: WebSocket):
        await websocket.close(code=401)

    async def broadcast(
            self,
            users_ids: List[str],
            response: WSResponse,
    ):
        assert isinstance(response, WSResponse)
        tasks = [
            self.broadcaster.publish(
                channel=str(user_id),
                message=response.json()
            )
            for user_id
            in users_ids
        ]
        await asyncio.gather(*tasks)

    @staticmethod
    async def send_personal_message(
            websocket: WebSocket,
            response: Union[WSResponse, str]
    ):
        await websocket.send_text(response if isinstance(response, str) else response.json())
