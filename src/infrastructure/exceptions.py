from fastapi import HTTPException
from starlette import status


class InfrastructureException(HTTPException):
    ...


class NotAuthorized(InfrastructureException):
    def __init__(self):
        detail = 'User not authorized.'
        super().__init__(status.HTTP_401_UNAUTHORIZED, detail)


class NotFound404(InfrastructureException):
    def __init__(self):
        detail = 'Not found.'
        super().__init__(status.HTTP_404_NOT_FOUND, detail)
