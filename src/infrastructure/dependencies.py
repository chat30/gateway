from dataclasses import dataclass

from src.infrastructure.exceptions import NotAuthorized

from fastapi import Request


@dataclass
class UserInfo:
    id: str


def get_request_user(request: Request) -> UserInfo:
    try:
        print(request.state)
        user_info = getattr(request.state, 'user_info')
    except AttributeError:
        print(111)
        raise NotAuthorized()
    return UserInfo(**user_info)
