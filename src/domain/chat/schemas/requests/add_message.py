from pydantic import BaseModel, constr


class MessageData(BaseModel):
    text: constr(strip_whitespace=True, max_length=600)


class AddMessageRequest(BaseModel):
    chat_id: str
    message_data: MessageData
