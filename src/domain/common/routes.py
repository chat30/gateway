from starlette.concurrency import run_until_first_complete
from starlette.websockets import WebSocket
from fastapi import Query, APIRouter

from src.infrastructure.auth_service import decode_token, Tokens
from src.infrastructure.exceptions import NotAuthorized
from src.infrastructure.ws.connections_manager import ConnectionManager
from src.infrastructure.ws.ws_handler import process_websockets_messages, subscribe_user_topic

ws_router = APIRouter(prefix='/ws/v1')
ws_manager = ConnectionManager()


@ws_router.websocket('/connection')
async def chat_websocket(
        websocket: WebSocket,
        ws_token: str = Query(..., alias='ws_token'),
):
    decoded = decode_token(ws_token, Tokens.WS_TOKEN)
    user_id = decoded['id']
    if not user_id:
        raise NotAuthorized()

    await ws_manager.connect(
        websocket=websocket,
        user_id=user_id,
    )

    process_websockets_messages_data = {
        'websocket': websocket,
        'user_id': user_id,
    }
    subscribe_user_topic_data = {
        'websocket': websocket,
        'user_id': user_id,
    }
    await run_until_first_complete(
        (process_websockets_messages, process_websockets_messages_data),
        (subscribe_user_topic, subscribe_user_topic_data),
    )
