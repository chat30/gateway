import abc
from typing import Type, Callable, Coroutine

from pydantic import BaseModel


class UseCase(metaclass=abc.ABCMeta):
    request_schema: Type[BaseModel] | None = None
    response_schema: Type[BaseModel] | None = None
    _run: Callable

    def __init__(self, input_data: dict, user_id: str | None, *args, **kwargs):
        self.input_data = input_data
        self.user_id = user_id
        self._validate_request()
        self.result = None

    def _validate_request(self):
        if self.request_schema:
            self.validated_request = self.request_schema(**self.input_data)
        else:
            self.validated_request = None

    def _validate_response(self):
        if self.request_schema:
            self.result = self.response_schema(**self.input_data)

    def run(self) -> dict | None | Coroutine:
        self.result = self._run()
        self._validate_response()
        return self.result
