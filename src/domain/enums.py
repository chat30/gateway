from src.infrastructure.types import StrEnum


class Services(StrEnum):
    USERS = 'users'
    CHAT = 'chat'
