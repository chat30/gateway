### Установить docker
https://docs.docker.com/engine/install/

### Установить kubectl
https://kubernetes.io/docs/tasks/tools/

### Установить minikube
https://minikube.sigs.k8s.io/docs/start/

### Установить helm
https://helm.sh/docs/intro/install/

### Установить средство виртуализации.
Опционально, но желательно, установить средства виртуализации для minikube, для mac не взлетает запуск миникуба под управлением докера. У меня нормально и полноценно заработало только с hyperkit.
`brew install hyperkit`
P.S. в линухе достаточно просто docker

### Запускаем кластер миникуба:
`minikube start --vm-driver=hyperkit --memory 8192 --cpus 3`

либо, если используешь чисто докер (линух)

`minikube start --memory 8192 --cpus 3`

### Переключаем докер на работу внутри миникуба:
`eval $(minikube docker-env)`

### Переходим в папку с проектом. Из папки с проектом запускаем сборку образа:
`docker build -f Dockerfile.api -t zaitsevartem/example-gateway .`

### Подключаем аддоны для ингресса:
`minikube addons enable ingress`
`minikube addons enable ingress-dns`

### Секреты
В файле `deployment/secrets.yaml` нужно заполнить секреты. Секреты принимаются в base64 формате. Питонячий код для этого:
```
import base64
base64.b64encode('secret_str'.encode('ascii'))
```

### Разворачиваем кубер-манифесты используя helm.
`helm install gateway-app deploy`

### DNS
Для того, чтобы использовать доменную запись внутри кубера, нужно подкинуть локально запись внутрь /etc/hosts. Для этого получаем ip ingress'а:

`nslookup appexample.hack $(minikube ip)`

Либо берем конкретно айдишник прописанный у ingress'а:

`kubectl get ing fitingo-ingress`

Открываем файл:
`sudo nano /etc/hosts`

Дописываем запись внутрь файла:

`<полученный выше ip> fitingoapp.hack`

Чистим кеш DNS (не обязательно, но может понадобиться):

`sudo killall -HUP mDNSResponder`

В отдельном терминале можно запустить дефолтный дашборд для удобного чего-нибудь:
`minikube dashboard`

Удалить всё что есть в кластере `helm uninstall gateway-app`.
